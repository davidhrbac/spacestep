# SpaceStep Project

![logo](/images/logo43.png "Project logo")

SpaceStep serves as a streamlined, budget-friendly alternative to Spacewalk, offering a simple yet effective web application designed for real-time system management and monitoring. Developed using Flask, SpaceStep includes a client application scripted in Bash. This client automates system checks and updates, executing on an hourly basis, upon system boot, and following any DNF transaction.

The SpaceStep system performs a variety of critical checks:

- Verifies the latest kernel version.
- Confirms the current OS release.
- Determines the necessity for system reboots or service restarts.
- Checks the status of automatic update provisioning via DNF Automatic.
- Monitors system uptime.
- Records host creation and update dates.
- Logs the date of the last communication with the server.

## Screenshots

Below is a screenshot showcasing the SpaceStep web application interface:

![SpaceStep Web App Screenshot](/images/screenshot.png "Screenshot of the SpaceStep web application")

## Client Installation Guide

### Pre-Installation Configuration

Ensure the `/etc/spacestep.conf` file exists and is correctly configured before proceeding with the installation or update. Use the provided sample configuration as a template:

```console
cat client/spacestep.conf.sample
# spacestep.conf.sample

# This is a sample configuration file for SpaceStep application.
# Copy this file to /etc/spacestep.conf and update the values according to your environment.

# API URL ROOT for connecting to the SpaceStep service
API_URL=http://example.com

# Bearer token for authenticating with the SpaceStep service
BEARER_TOKEN=ReplaceThisWithYourToken
```

### Basic Installation

To install the SpaceStep client:

```console
curl -s https://gitlab.com/davidhrbac/spacestep/-/raw/master/client/install.sh | sh -
```

For installations requiring DNF automatic updates:

```console
curl -s https://gitlab.com/davidhrbac/spacestep/-/raw/master/client/install.sh | SPACESTEP_AUTOUPDATE=true sh -
```

To update an existing client installation:

```console
curl -s https://gitlab.com/davidhrbac/spacestep/-/raw/master/client/install.sh | SPACESTEP_UPDATE=true sh -
```

### Installation Options

The install.sh script supports several options to customize the installation process:

```console
Usage: client/install.sh [OPTION]...
Enhances the system by installing and configuring specific packages.

Options:
  --autoupdate          Install and configure dnf-automatic.
  --no-dnfhook          Do not install and configure python3-dnf-plugin-post-transaction-actions.
  --no-id-check         Skip machine ID check.
  -h, --help            Display this help and exit.

Example:
  client/install.sh --no-autoupdate    Only skips the installation and configuration of dnf-automatic.
```

To apply a specific installation option:

```console
curl -s https://gitlab.com/davidhrbac/spacestep/-/raw/master/client/install.sh | bash -s -- [YOUR_SWITCH]
```

Environment variables can be used to override default installation behaviors:

* SPACESTEP_AUTOUPDATE=true - Forces the installation of the DNF automatic tool.
* SPACESTEP_UPDATE=true - Forces the installation without checking the machine ID for duplicates.

## Server installation

### Prerequisites

Ensure Python version 3.8 or higher is installed:

```console
# Install development tools and libraries
yum -y groupinstall "Development Tools"
yum -y install openssl-devel bzip2-devel libffi-devel xz-devel sqlite-devel

# Verify gcc installation
gcc --version

# Download and install Python 3.8
yum -y install wget
wget https://www.python.org/ftp/python/3.8.18/Python-3.8.18.tgz
tar xvf Python-3.8.18.tgz
cd Python-3.8.18
./configure --enable-optimizations
make altinstall
python3.8 -V
```

Create and activate a Python virtual environment:

```console
virtualenv venv -p python3.8
source venv/bin/activate
```

Configure the server environment and initialize the database:

```console
setsebool -P httpd_can_network_connect true

flask init
flask db migrate -m "Initial migration."
flask db upgrade
```

### Environment Variables

Server-side configuration requires setting environment variables:

* SPACESTEP_TOKEN=your_token_here - **Mandatory**. Token for pushing values to the API.
* SPACESTEP_TOKEN_READ=your_read_token_here - Optional. Token for reading values from the API. If unspecified, data can be read by anyone. If set, users must create a browser cookie named SPACESTEP_TOKEN_READ with the token value.

Creating the necessary cookie via JavaScript:

```javascript
// Create a new date instance for the expiration date, one year in the future
var date = new Date();
date.setFullYear(date.getFullYear() + 1); // Adds one year
var expires = "expires=" + date.toUTCString();

// Set the cookie with Secure and SameSite=Strict attributes
document.cookie = "SPACESTEP_TOKEN_READ=your_read_token_here; path=/; " + expires + "; Secure; SameSite=Strict";
```

## Container

```console
docker build -t spacestep .
docker run -d -p 5000:5000 -e SPACESTEP_TOKEN=your_token_here -e SPACESTEP_TOKEN_READ=your_read_token_here -v $(pwd)/sampledb:/spacestep/instance spacestep
```

Open browser at URL http://127.0.0.1:5000/live
