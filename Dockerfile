FROM python:3.10-alpine

ADD . /spacestep

WORKDIR /spacestep

RUN pip install --progress-bar off -r requirements.txt

RUN pip install --progress-bar off gunicorn

EXPOSE 5000

#CMD gunicorn --worker-class gevent --workers 8 --bind 0.0.0.0:5000 wsgi:app --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info
#CMD flask db upgrade && gunicorn -m 007 --bind 0.0.0.0:5000 app:app
CMD gunicorn -m 007 --bind 0.0.0.0:5000 app:app
