#!/bin/sh
VERSION=20240306125629

# Initialize flags based on switches
INSTALL_AUTOUPDATE=false
INSTALL_DNFHOOK=true
SKIP_MACHINE_ID_CHECK=false

# Function to display help
show_help() {
    echo "Usage: $0 [OPTION]..."
    echo "Enhances the system by installing and configuring specific packages."
    echo ""
    echo "Options:"
    echo "  --autoupdate          Install and configure dnf-automatic."
    echo "  --no-dnfhook          Do not install and configure python3-dnf-plugin-post-transaction-actions."
    echo "  --no-id-check         Skip machine ID check."
    echo "  -h, --help            Display this help and exit."
    echo ""
    echo "Example:"
    echo "  $0 --no-autoupdate    Only skips the installation and configuration of dnf-automatic."
}

# Parse command-line arguments
for arg in "$@"
do
    case $arg in
        --autoupdate)
        INSTALL_AUTOUPDATE=true
        shift # Remove --autoupdate from processing
        ;;
        --no-dnfhook)
        INSTALL_DNFHOOK=false
        shift # Remove --no-dnfhook from processing
        ;;
        --no-id-check)
        SKIP_MACHINE_ID_CHECK=true
        shift # Remove --no-id-check from processing
        ;;
        -h|--help)
        show_help
        exit 0
        ;;
    esac
done

# Check for the SPACESTEP_AUTOUPDATE environment variable
if [ "${SPACESTEP_AUTOUPDATE}" = "true" ]; then
    INSTALL_AUTOUPDATE=true
fi

# Check for the SPACESTEP_UPDATE environment variable to skip machine ID check
if [ "$SPACESTEP_UPDATE" = "true" ]; then
    SKIP_MACHINE_ID_CHECK=true
fi

# Check for the presence of the config file
CONFIG_FILE="/etc/spacestep.conf"
if [ ! -f "$CONFIG_FILE" ]; then
    echo "Configuration file $CONFIG_FILE not found."
    exit 1
fi

# Source the configuration file
source "$CONFIG_FILE"

# Check if the API URL and Bearer token have been set
if [ -z "$API_URL" ] || [ -z "$BEARER_TOKEN" ]; then
    echo "API URL or Bearer token not set in $CONFIG_FILE."
    exit 1
fi

# Only perform machine ID check if --no-id-check is not specified and SPACESTEP_UPDATE is not true
if [ "$SKIP_MACHINE_ID_CHECK" = false ]; then
    # Machine ID
    MACHINE_ID=$(cat /etc/machine-id)

    # Construct check URL
    CHECK_URL="${API_URL}/api/check-machine-id/${MACHINE_ID}"

    # Make the request and capture the HTTP status code
    HTTP_STATUS=$(curl -s -o /dev/null -w "%{http_code}" -H "Authorization: Bearer $BEARER_TOKEN" "$CHECK_URL")

    # Check the HTTP status code
    if [ "$HTTP_STATUS" -eq 200 ]; then
        echo "Machine ID exists in the database."
        exit 1
    elif [ "$HTTP_STATUS" -eq 404 ]; then
        echo "Machine ID does not exist in the database."
        # Proceed with adding the machine ID to the database or other actions
    else
        echo "Error or unexpected response code: $HTTP_STATUS."
        exit 1
        # Handle other HTTP errors or unexpected responses
    fi
else
    echo "Skipping machine ID check as per --no-id-check flag or SPACESTEP_UPDATE environment variable."
fi

# Download the script and save it to /etc/cron.hourly/spacestep
curl -o /etc/cron.hourly/spacestep -sSL https://gitlab.com/davidhrbac/spacestep/-/raw/master/client/client.sh

# Set execute bit on the downloaded script
chmod +x /etc/cron.hourly/spacestep

# Create the cron.d file
echo "@reboot root /etc/cron.hourly/spacestep" | tee /etc/cron.d/spacestep >/dev/null

# Check if the OS is RPM-based (Rocky, RHEL, or CentOS)
if [ -f /etc/redhat-release ]; then
    # For RPM-based systems
    echo "RPM-based system detected."

    # Determine OS version
    OS_VERSION=$(rpm -E %{rhel})

    # Check for compatible version (8 or 9)
    if [ "$OS_VERSION" = "8" ] || [ "$OS_VERSION" = "9" ]; then
        echo "Compatible OS version detected: $OS_VERSION. Proceeding with installations."

        # Conditionally install python3-dnf-plugin-post-transaction-actions
        if [ "$INSTALL_DNFHOOK" = true ]; then
            echo "Installing python3-dnf-plugin-post-transaction-actions..."
            dnf install -y python3-dnf-plugin-post-transaction-actions

            # Configure the action file
            echo "Configuring action file..."
            echo "*:any: /etc/cron.hourly/spacestep" > /etc/yum/pluginconf.d/post-transaction-actions.d/spacestep.action
            echo "Action file configured successfully."
        fi

        # Conditionally install dnf-automatic
        if [ "$INSTALL_AUTOUPDATE" = true ]; then
            echo "Installing dnf-automatic..."
            dnf install -y dnf-automatic

            # Prepare dnf-automatic configuration
            echo "Configuring dnf-automatic..."
            # Example configuration adjustments
            sed -i 's/^upgrade_type.*$/upgrade_type = default/' /etc/dnf/automatic.conf
            sed -i 's/^apply_updates.*$/apply_updates = yes/' /etc/dnf/automatic.conf
            sed -i 's/^download_updates.*$/download_updates = yes/' /etc/dnf/automatic.conf
            echo "dnf-automatic configured successfully. Updates will be applied automatically."
            # Start and enable dnf-automatic service
            sudo systemctl enable --now dnf-automatic.timer
            echo "dnf-automatic service enabled and started."
        fi

    else
        echo "Incompatible OS version. This script is only compatible with Rocky/RHEL/CentOS 8 and 9."
    fi

    # Check if needs-restarting command is available
    if ! command -v needs-restarting &> /dev/null; then
        echo "needs-restarting command not found. Installing yum-utils package."
        yum install -y yum-utils
    fi
else
    echo "Non-RPM-based system detected."
fi

cat <<'EOF' > /usr/local/sbin/uninstall-spacestep.sh
#!/bin/sh
# This script will remove all files created by the spacestep installer

# Remove the cron job
rm -f /etc/cron.d/spacestep

# Remove the script downloaded to cron.hourly
rm -f /etc/cron.hourly/spacestep

# Remove the action file, if it was created
rm -f /etc/yum/pluginconf.d/post-transaction-actions.d/spacestep.action

# If dnf-automatic was installed, inform the user to manually uninstall if desired
echo "If dnf-automatic was installed and is no longer needed, you can uninstall it with:"
echo "dnf remove -y dnf-automatic"

# If python3-dnf-plugin-post-transaction-actions was installed, inform the user to manually uninstall if desired
echo "If python3-dnf-plugin-post-transaction-actions was installed and is no longer needed, you can uninstall it with:"
echo "dnf remove -y python3-dnf-plugin-post-transaction-actions"

# Completion message
echo "Uninstallation complete. Please manually remove any additional related files if necessary."

# Self-removal of the uninstaller script
rm -f /usr/local/sbin/uninstall-spacestep.sh

EOF

# Make the uninstaller executable
chmod +x /usr/local/sbin/uninstall-spacestep.sh

echo "Uninstaller created at /usr/local/sbin/uninstall-spacestep.sh"
