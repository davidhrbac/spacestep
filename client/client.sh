#!/bin/bash
VERSION=20240306134505

# Initialize DEBUG flag to false
DEBUG=false

# Source proxy settings if the file exists
if [ -f /etc/profile.d/proxy.sh ]; then
    source /etc/profile.d/proxy.sh
fi

# Check for the presence of the config file
CONFIG_FILE="/etc/spacestep.conf"
if [ ! -f "$CONFIG_FILE" ]; then
    echo "Configuration file $CONFIG_FILE not found."
    exit 1
fi

# Source the configuration file
source "$CONFIG_FILE"

# Check if the API URL and Bearer token have been set
if [ -z "$API_URL" ] || [ -z "$BEARER_TOKEN" ]; then
    echo "API URL or Bearer token not set in $CONFIG_FILE."
    exit 1
fi

# Use the API URL and Bearer token in the script
URL="${API_URL}/api/servers"
TOKEN="Bearer $BEARER_TOKEN"

# Check for the --debug flag
for arg in "$@"
do
    if [ "$arg" == "--debug" ]; then
        DEBUG=true
        break
    fi
done

# Collect server data
NAME=$(hostname -f)
DOMAIN=$(hostname -d)
OS_VERSION=$(grep PRETTY_NAME /etc/os-release | cut -d= -f2 | tr -d '"')
KERNEL_VERSION=$(uname -r)
SERVER_ID=$(cat /etc/machine-id)
BOOTED_AT=$(date -d @$(vmstat --stats | awk '/boot time/{print $1}') +"%Y-%m-%dT%H:%M:%S" --utc)
NEEDS_RESTARTING=false

# Function to check if dnf-automatic is configured to perform automatic upgrades
# and if the corresponding service is enabled and active
is_dnf_automatic_enabled() {
    # Check if the configuration file exists
    if [ -f /etc/dnf/automatic.conf ]; then
        # Check if the configuration file contains the "upgrade_type" option set to "default"
        if grep -q '^upgrade_type\s*=\s*default' /etc/dnf/automatic.conf; then
            # Check if the dnf-automatic service is enabled and active
            if systemctl is-enabled --quiet dnf-automatic.timer && systemctl is-active --quiet dnf-automatic.timer; then
                return 0  # Automatic upgrades are enabled and the service is active
            else
                return 1  # Automatic upgrades are enabled but the service is not active or enabled
            fi
        else
            return 2  # Automatic upgrades are not enabled
        fi
    else
        return 3  # Configuration file does not exist
    fi
}

if [ -f /etc/redhat-release ]; then
    if command -v needs-restarting &> /dev/null; then
        needs-restarting -r &> /dev/null
        if [ $? -eq 1 ]; then
            NEEDS_RESTARTING=true
        fi
    else
        if $DEBUG; then echo "needs-restarting command not found. Cannot determine restart requirement."; fi
    fi

    # Function call and capturing its return status
    is_dnf_automatic_enabled
    DNF_AUTOMATIC_STATUS=$?

    # Debug messages based on the return status
    case $DNF_AUTOMATIC_STATUS in
        0) if $DEBUG; then echo "Automatic upgrades are enabled and the service is active."; fi ;;
        1) if $DEBUG; then echo "Automatic upgrades are enabled but the service is not active or enabled."; fi ;;
        2) if $DEBUG; then echo "Automatic upgrades are not enabled."; fi ;;
        3) if $DEBUG; then echo "Configuration file does not exist."; fi ;;
        *) if $DEBUG; then echo "Unexpected status code from dnf automatic check."; fi ;;
    esac
elif [ -f /var/run/reboot-required ]; then
    NEEDS_RESTARTING=true
fi

# Prepare JSON data for POST request including DNF_AUTOMATIC_STATUS
JSON_DATA=$(cat <<EOF
{
  "name": "$NAME",
  "domain": "$DOMAIN",
  "id": "$SERVER_ID",
  "os_version": "$OS_VERSION",
  "kernel_version": "$KERNEL_VERSION",
  "needs_restarting": $NEEDS_RESTARTING,
  "booted_at": "$BOOTED_AT",
  "dnf_automatic_status": $DNF_AUTOMATIC_STATUS
}
EOF
)

# Function to make curl request with or without debug mode
make_curl_request() {
    if $DEBUG; then
        # Make the POST request with verbose output
        curl -X POST "$URL" \
             -H "Content-Type: application/json" \
             -H "Authorization: $TOKEN" \
             -d "$JSON_DATA"
    else
        # Make the POST request silently
        curl -s -o /dev/null -X POST "$URL" \
             -H "Content-Type: application/json" \
             -H "Authorization: $TOKEN" \
             -d "$JSON_DATA"
    fi
}

# Use the DEBUG flag to control the request verbosity
make_curl_request
