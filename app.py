from flask import Flask, request, jsonify, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
from sqlalchemy import func
import os

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///servers.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_TOKEN'] = os.getenv('SPACESTEP_TOKEN', 'YourSecretTokenHere')
app.config['SECRET_TOKEN_READ'] = os.getenv('SPACESTEP_TOKEN_READ')

def get_version():
    try:
        # Read the version from the file
        with open('VERSION', 'r') as file:
            return file.read().strip()
    except IOError:
        # If the file does not exist or cannot be read
        return "N/A"

app.config['VERSION'] = get_version()

db = SQLAlchemy(app)
migrate = Migrate(app, db)  # Initialize Flask-Migrate

class Server(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    domain = db.Column(db.String(255))
    server_id = db.Column(db.String(100), unique=True)  # Ensure server_id is unique
    os_version = db.Column(db.String(50))
    kernel_version = db.Column(db.String(50))
    needs_restarting = db.Column(db.Boolean)
    dnf_automatic_status = db.Column(db.Integer)
    booted_at = db.Column(db.DateTime)  # Add boot time column
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

class ServerCommunication(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    server_id = db.Column(db.Integer, db.ForeignKey('server.id'), nullable=False)
    communication_time = db.Column(db.DateTime, default=datetime.utcnow)

    server = db.relationship('Server', backref=db.backref('communications', lazy=True))

@app.route('/api/check-machine-id/<machine_id>', methods=['GET'])
def check_machine_id(machine_id):
    # Get the Authorization header
    auth_header = request.headers.get('Authorization')

    # Validate the Authorization header
    if not auth_header or not auth_header.startswith('Bearer '):
        return jsonify({'message': 'Unauthorized, token missing or invalid'}), 401

    # Extract the token from the header
    token = auth_header.split(' ')[1]

    # Compare the provided token with the expected token
    if token != app.config['SECRET_TOKEN']:
        return jsonify({'message': 'Unauthorized, token incorrect'}), 401

    # Query the database for the provided machine ID
    server = Server.query.filter_by(server_id=machine_id).first()

    # If the server exists, return a message indicating the machine ID is already in the database
    if server:
        return jsonify({'exists': True, 'message': 'Machine ID is already in the database.'}), 200
    else:
        # If not found, return a message indicating the machine ID is not in the database
        return jsonify({'exists': False, 'message': 'Machine ID is not in the database.'}), 404

@app.route('/api/servers', methods=['POST'])
def add_or_update_server():
    auth_header = request.headers.get('Authorization')
    if not auth_header or not auth_header.startswith('Bearer '):
        return jsonify({'message': 'Unauthorized, token missing or invalid'}), 401
    token = auth_header.split(' ')[1]  # Extract the token part of the header
    if token != app.config['SECRET_TOKEN']:
        return jsonify({'message': 'Unauthorized, token incorrect'}), 401

    data = request.json

    # Convert boot time string to datetime object
    booted_at_str = data.get('booted_at', '')  # Get boot time string from JSON data
    if booted_at_str:
        try:
            booted_at = datetime.strptime(booted_at_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            booted_at = None  # Set boot time to None if parsing fails
    else:
        booted_at = None  # Set boot time to None if not provided

    dnf_automatic_status = data.get('dnf_automatic_status', -1)  # Default to -1 or another value indicating "unknown" or "not provided"

    server = Server.query.filter_by(server_id=data['id']).first()
    if server:
        # Update existing record
        server.name = data['name']
        #server.domain = data.get('domain','')
        server.domain = ".".join(data['name'].split('.')[::-1])
        server.os_version = data['os_version']
        server.kernel_version = data['kernel_version']
        server.needs_restarting = data['needs_restarting']
        server.booted_at = booted_at
        server.dnf_automatic_status = dnf_automatic_status

    else:
        # Create a new record
        server = Server(name=data['name'], domain=data.get('domain',''), server_id=data['id'], os_version=data['os_version'],
                            kernel_version=data['kernel_version'], needs_restarting=data['needs_restarting'],
                            booted_at=booted_at, dnf_automatic_status=dnf_automatic_status)  # Set the boot time
        db.session.add(server)
        db.session.flush()  # Flush to assign an ID to the new server

    # Check if a communication record already exists
    communication = ServerCommunication.query.filter_by(server_id=server.id).first()
    if communication:
        # If it exists, update the existing record's timestamp
        communication.communication_time = datetime.utcnow()
    else:
        # If no record exists, create a new one
        communication = ServerCommunication(server_id=server.id)
        db.session.add(communication)

    db.session.commit()
    return jsonify({'message': 'Server data processed successfully!'}), 200

@app.route('/api/server-status')
def server_status():
    # If SECRET_TOKEN is set, check for a matching cookie
    if app.config['SECRET_TOKEN_READ']:
        # Attempt to retrieve the secret token from the cookies
        token = request.cookies.get('SPACESTEP_TOKEN_READ')
        if not token or token != app.config['SECRET_TOKEN_READ']:
            # If the token is missing or doesn't match, deny access
            return jsonify({'error': 'Access denied. Invalid or missing token.'}), 401

    # Query to get the servers data and maximum updated_at and last_communication_at values
    servers_query = db.session.query(
        Server.name,
        Server.domain,
        Server.needs_restarting,
        Server.os_version,
        Server.kernel_version,
        Server.dnf_automatic_status,
        Server.booted_at,
        Server.created_at,
        Server.updated_at,
        ServerCommunication.communication_time.label('last_communication_at')
    ).order_by(Server.domain
    ).outerjoin(
        ServerCommunication, Server.id == ServerCommunication.server_id
    )

    # Get the maximum updated_at and last_communication_at values
    max_updated_at = servers_query.with_entities(func.max(Server.updated_at)).scalar()
    max_last_communication_at = servers_query.with_entities(func.max(ServerCommunication.communication_time)).scalar()

    # Get the servers data
    servers_data = servers_query.all()

    # Convert data to a list of dicts to be JSON serializable
    servers_response = [{'name': server.name,
                         'domain': server.domain,
                         'needs_restarting': server.needs_restarting,
                         'os_version': server.os_version,
                         'kernel_version': server.kernel_version,
                         'dnf_automatic_status': server.dnf_automatic_status,
                         'booted_at': server.booted_at.strftime('%Y-%m-%d %H:%M:%S') if server.booted_at else None,
                         'created_at': server.created_at.strftime('%Y-%m-%d %H:%M:%S'),
                         'updated_at': server.updated_at.strftime('%Y-%m-%d %H:%M:%S'),
                         'last_communication_at': server.last_communication_at.strftime('%Y-%m-%d %H:%M:%S') if server.last_communication_at else 'N/A'}
                        for server in servers_data]

    # Return the servers data and maximum values in the response
    return jsonify({'servers': servers_response,
                    'max_updated_at': max_updated_at.strftime('%Y-%m-%d %H:%M:%S') if max_updated_at else None,
                    'max_last_communication_at': max_last_communication_at.strftime('%Y-%m-%d %H:%M:%S') if max_last_communication_at else None,
                    'version': app.config['VERSION'] if app.config['VERSION'] else None})

@app.route('/', methods=['GET'])
def index():
    return render_template('live.html', version = app.config['VERSION'])

@app.route('/live', methods=['GET'])
def live():
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=False)

